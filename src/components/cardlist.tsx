import React from 'react';
import { CardsItem } from './cardsitem';
import { Card } from './interface';
import { listAnimal } from './listanimal';

interface Props { }
interface State { animalsList: Card[]; cardPicked: boolean; userAnimal: string; }

const newListAnimal = JSON.parse(JSON.stringify(listAnimal));

export class NewCardList extends React.Component<Props, State> {

     constructor(props: Props) {
          super(props);
          this.state = { animalsList: listAnimal, cardPicked: false, userAnimal: '' };
     }

     public render() {
          return <section className='container card'>
               <h1>Guess who? Animal Crossing</h1>
               {(!this.state.cardPicked) ?
                    <form onSubmit={(this.selectUserAnimal)}>
                         <label><h2>Choisi ton villageois:</h2></label>
                         <select value={this.state.userAnimal} className='avatar-select' required={true} onChange={(event) => this.setState({ userAnimal: event.target.value })}>
                              <option disabled={true} />
                              {this.state.animalsList.map(name => <option value={name.link} key={name.name}>{name.name}</option>)}
                         </select>
                         {this.state.userAnimal !== '' ? <div className='select-image'><img src={this.state.userAnimal} /></div> : ''}
                         <div className='center'><button className='btn'>Valider</button></div>
                    </form>
                    :
                    <div>
                         <div className='card-player-select'><img src={this.state.userAnimal} /></div>
                         <div className='flex card-section'>
                              {this.state.animalsList.map(newAnimal =>
                                   <CardsItem key={newAnimal.name} animal={newAnimal}
                                        cardSelected={() => this.cardSelect(newAnimal)}
                                   />)}
                         </div>
                         <div className='card-footer'>
                              <button className='btn' onClick={() => this.setState({ animalsList: JSON.parse(JSON.stringify(newListAnimal)), cardPicked: false, userAnimal: '' })}>Nouvelle partie</button>
                         </div>
                    </div>
               }
          </section >;
     }

     private selectUserAnimal = (event: React.FormEvent) => {
          event.preventDefault();
          this.setState({ cardPicked: true, userAnimal: this.state.userAnimal });
     };

     private cardSelect = (cardSwitch: Card) => {
          cardSwitch.correct = !cardSwitch.correct;
          this.setState({ animalsList: this.state.animalsList });
     };


}

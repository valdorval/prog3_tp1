import React from 'react';
import { Card } from './interface';

interface Props { animal: Card; cardSelected(): void; }
interface State { }

export class CardsItem extends React.Component<Props, State> {

     public render() {
          return <div className={this.props.animal.correct ? 'card-image-select' : 'card-image'} onClick={this.props.cardSelected}>
               <img className='npc' src={this.props.animal.link} alt={this.props.animal.name} />
               <span>{this.props.animal.name}</span>
          </div>;
     }
}

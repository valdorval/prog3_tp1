export interface Card {
     name: string;
     link: string;
     correct?: boolean;
}

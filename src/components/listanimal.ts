import { Card } from './interface';

export const listAnimal: Card[] = [{
        name: 'Mathéo',
        link: 'img/matheo.png',
},
{
        name: 'Irène',
        link: 'img/irene.png'
},
{
        name: 'Anna',
        link: 'img/anna.png'
},
{
        name: 'Bibi',
        link: 'img/bibi.png'
},
{
        name: 'Blanche',
        link: 'img/blanche.png'
},
{
        name: 'Bouloche',
        link: 'img/bouloche.png'
},
{
        name: 'Capri',
        link: 'img/capri.png'
},
{
        name: 'Charlène',
        link: 'img/charlene.png'
},
{
        name: 'Chavrina',
        link: 'img/chavrina.png'
},
{
        name: 'Jennifer',
        link: 'img/jennifer.png'
},
{
        name: 'Ketchup',
        link: 'img/ketchup.png'
},
{
        name: 'Kristine',
        link: 'img/kristine.png'
},
{
        name: 'Laura',
        link: 'img/laura.png'
},
{
        name: 'Lico',
        link: 'img/lico.png'
},
{
        name: 'Marina',
        link: 'img/marina.png'
},
{
        name: 'Marvin',
        link: 'img/marvin.png'
},
{
        name: 'Miro',
        link: 'img/miro.png'
},
{
        name: 'Molly',
        link: 'img/molly.png'
},
{
        name: 'Nacer',
        link: 'img/nacer.png'
},
{
        name: 'Nadine',
        link: 'img/nadine.png'
},
{
        name: 'Neferti',
        link: 'img/neferti.png'
},
{
        name: 'Pierrot',
        link: 'img/pierrot.png'
},
{
        name: 'Ramsès',
        link: 'img/ramses.png'
},
{
        name: 'Raymond',
        link: 'img/raymond.png'
}

];

import React from 'react';
import { Link } from 'react-router-dom';

interface Props { }
interface State { }

export class Presentation extends React.Component<Props, State> {

     public render() {
          return <section className='container card'>
               <h1>Guess who? Animal crossing</h1>
               <div className='container'>
                    <div className='center'><img src='img/background.jpg' alt='background animal crossing' /></div>
                    <h2>Bienvenue sur le nouveau jeu en ligne Guess Who?  <br /> Animal Crossing édition!</h2>
                    <p>Viens t'amuser avec nous! Les règles sont simples:</p>
                    <p>Chaque joueur a un tableau de 24 images et noms des différents villageois sur 4 rangées et 6
                    colonnes. Chaque joueur choisit d'abord le villageois qu'il désire incarné. Les joueurs se posent alors des questions l’un
                    après l’autre pour deviner le personnage de son adversaire. Les joueurs éliminent les personnages
                    qui ne sont définitivement pas le personnage de l’autre joueur jusqu’à ce qu’il n’en reste plus qu’un. La
première personne à deviner le personnage de l’autre gagne la partie.</p>
                    <Link to='/newgame'><div className='center'><button className='btn'>Jouer</button></div></Link>
               </div>

          </section >;
     }


}

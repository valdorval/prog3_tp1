import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import { NewCardList } from './cardlist';
import { Presentation } from './presentation';

export class Router extends React.Component<{}> {
     public render() {
          return < BrowserRouter>
               <Route path='/newgame' exact={true}> <NewCardList /> </Route>
               <Route path='/' exact={true}> <Presentation /> </Route>

          </ BrowserRouter>;
     }
}
